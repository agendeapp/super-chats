export var __esModule: boolean;
export default FluidDB;
declare class FluidDB {
    constructor(key: any, id: any);
    key: any;
    idGetter: any;
    dict: {};
    array: any[];
    get length(): number;
    get first(): any;
    get last(): any;
    toJSON(): any[];
    insert(...values: any[]): void;
    upsert(...values: any[]): any[];
    insertIfAbsent(...values: any[]): any[];
    deleteById(id: any, assertPresent?: boolean): any;
    delete(value: any): any;
    slice(start: any, end: any): FluidDB;
    /** Clears the DB */
    clear(): void;
    get(id: any): any;
    all(): any[];
    update(id: any, update: any): 1 | 2;
    updateKey(value: any, update: any): 1 | 2;
    filter(predicate: any): FluidDB;
    paginatedByValue(value: any, limit: any, predicate: any, mode?: string): any[];
    paginated(cursor: any, limit: any, predicate: any, mode?: string): any[];
    _insertSingle(value: any): void;
    filtered(start: any, count: any, mode: any, predicate: any): any[];
    firstIndex(value: any): any;
    [Symbol.iterator](): Generator<any, void, unknown>;
}
