import { SuperChats as Base } from './load_messages';
declare type listSettings = ('message' | 'settings');
export declare class SuperChats extends Base {
    createGroup(name: string, participants: any): Promise<{}>;
    addParticipantsGroup(id: string, participants: any): Promise<{}>;
    removeParticipantsGroup(id: string, participants: any): Promise<{}>;
    addGroupAdmins(id: string, participants: any): Promise<{}>;
    removeGroupAdmins(id: string, participants: any): Promise<{}>;
    groupTitle(id: string, title: string): Promise<{}>;
    groupDescription(id: string, description: string): Promise<{}>;
    leaveGroup(id: string): Promise<{}>;
    getGroupLink(id: string): Promise<{}>;
    revokeGroupLink(id: string): Promise<{}>;
    joinGroup(code: string): Promise<{}>;
    infoGroup(id: string): Promise<{}>;
    setGroupSettings(id: string, option: listSettings, boolean: boolean): Promise<{}>;
}
export {};
