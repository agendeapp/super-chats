/// <reference types="node" />
import { SuperChats as Base } from './groups';
export declare class SuperChats extends Base {
    onMessage(listener: (...args: any[]) => void): Promise<this>;
    onAck(listener: (...args: any[]) => void): this;
    onPresence(listener: (...args: any[]) => void): this;
    onDelete(listener: (...args: any[]) => void): this;
    onParticipants(listener: (...args: any[]) => void): this;
    onGroups(listener: (...args: any[]) => void): this;
    protected base64Convert(buffer: any): any;
    protected inviteConflit(m: any): Promise<Buffer>;
}
