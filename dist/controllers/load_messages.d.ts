import { SuperChats as Base } from './functions';
export declare class SuperChats extends Base {
    getChatMessages(id: string, number: number): Promise<{}>;
    private eventMessagesList;
    getChatAllMessages(id: string): Promise<{}>;
    getAllUnreadMessages(): Promise<{}>;
    getMessageById(id: string, idMsg: string): Promise<{}>;
    forwardMessage(chatId: string, msgId: string, toId: string): Promise<{}>;
    protected removeUndefined(array: any): any[];
}
