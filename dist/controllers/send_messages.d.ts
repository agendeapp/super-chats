/// <reference types="node" />
import { SuperChats as Base } from './profile';
import { MessageOptions, MessageType, WALocationMessage, WAContactMessage, WAContactsArrayMessage, WAGroupInviteMessage, WAListMessage, WAButtonsMessage, WATextMessage, WAMessageContent, WAMessage, WAMessageProto, MediaConnInfo, WAMediaUpload } from './variables';
import { Readable } from 'stream';
export declare class SuperChats extends Base {
    sendMessage(id: string, message: string | WATextMessage | WALocationMessage | WAContactMessage | WAContactsArrayMessage | WAGroupInviteMessage | WAMediaUpload | WAListMessage | WAButtonsMessage, type: MessageType, options?: MessageOptions): Promise<WAMessageProto.WebMessageInfo>;
    sendListMessage(id: string, button: {
        buttonText?: string;
        description?: string;
        title?: string;
    }, rows?: any): Promise<WAMessageProto.WebMessageInfo>;
    prepareMessage(id: string, message: string | WATextMessage | WALocationMessage | WAContactMessage | WAContactsArrayMessage | WAGroupInviteMessage | WAMediaUpload | WAListMessage | WAButtonsMessage, type: MessageType, options?: MessageOptions): Promise<WAMessageProto.WebMessageInfo>;
    toggleDisappearingMessages(jid: string, ephemeralExpiration?: number, opts?: {
        waitForAck: boolean;
    }): Promise<void>;
    prepareMessageContent(message: string | WATextMessage | WALocationMessage | WAContactMessage | WAContactsArrayMessage | WAGroupInviteMessage | WAMediaUpload | WAListMessage | WAButtonsMessage, type: MessageType, options: MessageOptions): Promise<WAMessageProto.Message>;
    prepareDisappearingMessageSettingContent(ephemeralExpiration?: number): WAMessageProto.Message;
    prepareMessageMedia(media: WAMediaUpload, mediaType: MessageType, options?: MessageOptions): Promise<WAMessageProto.Message>;
    prepareMessageFromContent(id: string, message: WAMessageContent, options: MessageOptions): WAMessageProto.WebMessageInfo;
    relayWAMessage(message: WAMessage, { waitForAck }?: {
        waitForAck: boolean;
    }): Promise<void>;
    updateMediaMessage(message: WAMessage): Promise<void>;
    downloadMediaMessage(message: WAMessage): Promise<Buffer>;
    downloadMediaMessage(message: WAMessage, type: 'buffer'): Promise<Buffer>;
    downloadMediaMessage(message: WAMessage, type: 'stream'): Promise<Readable>;
    downloadAndSaveMediaMessage(message: WAMessage, filename: string, attachExtension?: boolean): Promise<string>;
    generateLinkPreview(text: string): Promise<WAMessageProto.ExtendedTextMessage>;
    protected refreshMediaConn(forceGet?: boolean): Promise<MediaConnInfo>;
    protected getNewMediaConn(): Promise<MediaConnInfo>;
}
