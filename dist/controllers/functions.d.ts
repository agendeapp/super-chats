import { SuperChats as Base } from './events';
export declare class SuperChats extends Base {
    private qrcode_all;
    pushQr(): Promise<void>;
    getQR(): Promise<{
        session: string;
        device: string;
        status: number;
        type: string;
        base64: any;
    } | {
        session: string;
        device: string;
        status: number;
        type: string;
        message: string;
    }>;
    getNumberProfile(id: string): Promise<{}>;
    getProfileStatus(id: string): Promise<{}>;
    getPicture(id: string): Promise<{}>;
    setPicture(id: string, file: string): Promise<{}>;
    archiveChat(id: string, boolean: boolean): Promise<{}>;
    pinChat(id: string, boolean: boolean): Promise<{}>;
    muteChat(id: string, timer: timerMute): Promise<{}>;
    unmuteChat(id: string): Promise<{}>;
    deleteChat(id: string): Promise<{}>;
    blockContact(id: string): Promise<{}>;
    unblockContact(id: string): Promise<{}>;
    deleteMessageAll(id: string, messageId: string): Promise<{}>;
    deleteMessageMe(id: string, messageId: string): Promise<{}>;
    getBlockList(): Promise<{}>;
    getAllContacts(): Promise<{}>;
    getBatteryLevel(): Promise<{}>;
    getHostDevice(): Promise<{}>;
    getChats(): Promise<{
        session: string;
        device: string;
        status: number;
        type: string;
        chats: any[];
    }>;
    getGroups(): Promise<{
        session: string;
        device: string;
        status: number;
        type: string;
        groups: any[];
    }>;
    decryptFile(message: any): Promise<{}>;
    decryptRemote(message: any): Promise<{}>;
    decryptByIdFile(id: string, msgId: string): Promise<{}>;
    decryptFileSave(message: any, filename: any): Promise<{}>;
    decryptByIdFileSave(id: string, msgId: string, filename: string): Promise<{}>;
    setPresence(id: string, type: string): Promise<{}>;
    forceStatusOn(): Promise<void>;
    getConnectionState(): Promise<{
        session: string;
        device: string;
        status: number;
        type: string;
        state: string;
    }>;
    markRead(id: string): Promise<{}>;
}
declare type timerMute = 'hour' | 'week' | 'ever';
export {};
