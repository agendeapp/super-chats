import { WAConnectOptions, WAOpenResult } from './variables';
import { SuperChats as Base } from './verify';
export declare class create extends Base {
    private session;
    protected listchats: any;
    protected addList: any;
    constructor(session?: string);
    /** Connect to WhatsApp Web */
    connect(): Promise<create>;
    /** Meat of the connect logic */
    protected connectInternal(options: WAConnectOptions, delayMs?: number): Promise<WAOpenResult>;
    eventEmiter(nameEvent: string): Promise<unknown>;
    private onMessageRecieved;
    /** Send a keep alive request every X seconds, server updates & responds with last seen */
    private startKeepAliveRequest;
}
